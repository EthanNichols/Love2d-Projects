
buildings = {}

buildingHover = -1
buildBuilding = ""

function buildings.load()
	townHall = love.graphics.newImage("images/townHall.png")
end

function buildings.build(building, mouseX, mouseY)
	for i, v in ipairs(plots) do
		if mouseX >= v.x and
		mouseX <= v.x + v.img:getWidth() and
		mouseY >= v.y and
		mouseY <= v.y + v.img:getHeight() then
			buildBuilding = building
			buildingHover = v.plotNum
			break
		else
			buildingHover = -1
		end
	end
end

function buildings.draw()
	if buildingHover >= 0 then
		for i, v in ipairs(plots) do
			if buildingHover == v.plotNum then
				buildBuilding:setFilter("nearest", "nearest")
				love.graphics.draw(buildBuilding, v.x + v.img:getWidth() / 2 - buildBuilding:getWidth() / 2, v.y + v.img:getHeight() / 2 - buildBuilding:getHeight() / 2)
			end
		end
	end
end