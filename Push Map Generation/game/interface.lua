
interface = {}

function interface.load()

	--load the images that create the interface
	bottomInterface = love.graphics.newImage("images/bottomInterface.png")

	--These images are for the minimap in the lower right hand corner
	miniMapBorder = love.graphics.newImage("images/miniMapBorder.png")
	miniMapCorner = love.graphics.newQuad(0, 0, 100, 100, miniMapBorder:getDimensions())
	miniMapTop = love.graphics.newQuad(100, 0, 100, 100, miniMapBorder:getDimensions())
	miniMapLeft = love.graphics.newQuad(0, 100, 100, 100, miniMapBorder:getDimensions())
	miniMapBlank = love.graphics.newQuad(100, 100, 100, 100, miniMapBorder:getDimensions())

	--Load the build tile image
	interfaceBuildTile = love.graphics.newImage("images/interfaceBuildTile.png")

	--Load all the buildings that can be built
	buildingIcons = love.graphics.newImage("images/buildingIcons.png")
	abilityUpgrade = love.graphics.newQuad(0, 0, 62, 62, buildingIcons:getDimensions())
	attackBuff = love.graphics.newQuad(62, 0, 62, 62, buildingIcons:getDimensions())
	attackUpgrade = love.graphics.newQuad(62 * 2, 0, 62, 62, buildingIcons:getDimensions())
	defenseBuff = love.graphics.newQuad(62 * 3, 0, 62, 62, buildingIcons:getDimensions())
	defenseUpgrade = love.graphics.newQuad(62 * 4, 0, 62, 62, buildingIcons:getDimensions())
	healthBuff = love.graphics.newQuad(62 * 5, 0, 62, 62, buildingIcons:getDimensions())
	healthUpgrade = love.graphics.newQuad(62 * 6, 0, 62, 62, buildingIcons:getDimensions())
	specialBuff = love.graphics.newQuad(62 * 7, 0, 62, 62, buildingIcons:getDimensions())
	specialUpgrade = love.graphics.newQuad(62 * 8, 0, 62, 62, buildingIcons:getDimensions())
	townHall = love.graphics.newQuad(62 * 9, 0, 62, 62, buildingIcons:getDimensions())

	healthRegenBuff = love.graphics.newQuad(0, 62, 62, 62, buildingIcons:getDimensions())
	criticalBuff = love.graphics.newQuad(62, 62, 62, 62, buildingIcons:getDimensions())
	criticalUpgrade = love.graphics.newQuad(62 * 2, 62, 62, 62, buildingIcons:getDimensions())
	rangeBuff = love.graphics.newQuad(62 * 3, 62, 62, 62, buildingIcons:getDimensions())
	rangeUpgrade = love.graphics.newQuad(62 * 4, 62, 62, 62, buildingIcons:getDimensions())
	accuracyBuff = love.graphics.newQuad(62 * 5, 62, 62, 62, buildingIcons:getDimensions())
	accuracyUpgrade = love.graphics.newQuad(62 * 6, 62, 62, 62, buildingIcons:getDimensions())

	--Create the canvas that contains all of the interface images
	interfaceCanvas = love.graphics.newCanvas(love.graphics.getWidth(), love.graphics.getHeight())
	buildInterfaceCanvas = love.graphics.newCanvas(love.graphics.getWidth(), love.graphics.getHeight())

	--Draw the images onto the canvas
	interface.drawCanvas()
end

function interface.drawCanvas()

	--Set the interface canvas to start being drawn on
	love.graphics.setCanvas(interfaceCanvas)

		--Set a local X and Y location on the canvas
		local x = 0
		local y = 0

		while x <= love.graphics.getWidth() do
			--Draw the lower portion of interface
			love.graphics.draw(bottomInterface, x, love.graphics.getHeight() - bottomInterface:getHeight())
			x = x + bottomInterface:getWidth()
		end

		--Set the new starting location for X and Y
		--Local variables to make a grid for the mini map border
		x = love.graphics.getWidth() - (love.graphics.getWidth() / 20) * 4
		y = love.graphics.getHeight() - (love.graphics.getWidth() / 20) * 4
		local column = 0 
		local row = 0

		--Repeat until the border goes outside the width and height of the screen
		repeat

			--Make sure that the X value is on the screen
			if x < love.graphics.getWidth() then
				--Test which row is currently being drawn
				--Test which column is currently being drawn
				if row == 0 then
					if column == 0 then
						--Draw the corner of the border
						love.graphics.draw(miniMapBorder, miniMapCorner, x, y)
					else
						--Draw the top of the border
						love.graphics.draw(miniMapBorder, miniMapTop, x, y)
					end
				else
					if column == 0 then
						--Draw the left side of the border
						love.graphics.draw(miniMapBorder,miniMapLeft, x, y)
					else
						--Draw the infill of the border, so no blank space
						love.graphics.draw(miniMapBorder, miniMapBlank, x, y)
					end
				end

				--Set the new X position
				--Increase the column that is being drawn to
				x = x + 100
				column = column + 1
			else
				--Reset the X X and column variables
				--Set the new Y value and increase the row that is currently being drawn
				x = love.graphics.getWidth() - (love.graphics.getWidth() / 20) * 4
				y = y + 100
				column = 0
				row = row + 1
			end

		until x > love.graphics.getWidth() and y > love.graphics.getHeight()

	--Unset the interface canvas from being drawn to
	love.graphics.setCanvas()

	--Set the canvas to start to be drawn on
	love.graphics.setCanvas(buildInterfaceCanvas)
		local buildTiles = 0
		local x = love.graphics.getWidth() - (love.graphics.getWidth() / 20) * 4 - 10 - 62
		local y = love.graphics.getHeight() - 152
			while buildTiles <= 16 do
				love.graphics.draw(interfaceBuildTile, x, y)

				if buildTiles == 0 then love.graphics.draw(buildingIcons, abilityUpgrade, x, y)
				elseif buildTiles == 1 then love.graphics.draw(buildingIcons, specialUpgrade, x, y)
				elseif buildTiles == 2 then love.graphics.draw(buildingIcons, specialBuff, x, y)
				elseif buildTiles == 3 then love.graphics.draw(buildingIcons, healthUpgrade, x, y)
				elseif buildTiles == 4 then love.graphics.draw(buildingIcons, healthBuff, x, y)
				elseif buildTiles == 5 then love.graphics.draw(buildingIcons, defenseUpgrade, x, y)
				elseif buildTiles == 6 then love.graphics.draw(buildingIcons, defenseBuff, x, y)
				elseif buildTiles == 7 then love.graphics.draw(buildingIcons, attackUpgrade, x, y)
				elseif buildTiles == 8 then love.graphics.draw(buildingIcons, attackBuff, x, y)
				elseif buildTiles == 9 then love.graphics.draw(buildingIcons, townHall, x, y)
				elseif buildTiles == 10 then love.graphics.draw(buildingIcons, healthRegenBuff, x, y)
				elseif buildTiles == 11 then love.graphics.draw(buildingIcons, criticalBuff, x, y)
				elseif buildTiles == 12 then love.graphics.draw(buildingIcons, criticalUpgrade, x, y)
				elseif buildTiles == 13 then love.graphics.draw(buildingIcons, rangeBuff, x, y)
				elseif buildTiles == 14 then love.graphics.draw(buildingIcons, rangeUpgrade, x, y)
				elseif buildTiles == 15 then love.graphics.draw(buildingIcons, accuracyBuff, x, y)
				elseif buildTiles == 16 then love.graphics.draw(buildingIcons, accuracyUpgrade, x, y)
				end
				x = x - interfaceBuildTile:getWidth() - 10
				buildTiles = buildTiles + 1
			end
	love.graphics.setCanvas()
end

function interface.draw()
	--Draw the interface canvas
	love.graphics.draw(interfaceCanvas, 0 - love.graphics.getWidth() / 2, 0 - love.graphics.getHeight() / 2)
	love.graphics.draw(buildInterfaceCanvas, 0 - love.graphics.getWidth() / 2, 0 - love.graphics.getHeight() / 2)
end